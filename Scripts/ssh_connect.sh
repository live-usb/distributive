#!/bin/ash
. rdp.sh
./vpn_connect.sh

if [[ "$SSH_USE_DOMAIN" == "yes" ]]; then
	gnome-terminal -- ssh "$SSH_DOMAIN"\\"$SSH_USERNAME"@"$SSH_IP" -p "$SSH_PORT"
fi
	
if [[ "$SSH_USE_DOMAIN" == "no" ]]; then
	gnome-terminal -- ssh "$SSH_USERNAME"@"$SSH_IP" -p "$SSH_PORT"
fi
