#!/bin/ash

# ETH0
echo -e "auto lo\niface lo inet loopback\n\nauto eth0\niface eth0 inet dhcp" >> /etc/network/interfaces
ifup lo
ifup eth0

# APK REPOSITORIES
echo "http://mirror.yandex.ru/mirrors/alpine/v3.18/main" >> /etc/apk/repositories
echo "http://mirror.yandex.ru/mirrors/alpine/v3.18/community" >> /etc/apk/repositories
apk update

# SSH
apk add openssh
rc-update add sshd
rc-service sshd start

# XFCE
setup-xorg-base
apk add xfce4 xfce4-terminal xfce4-screensaver lightdm-gtk-greeter dbus

rc-service dbus start
rc-update add dbus

rc-service udev start
rc-update add udev

rc-service lightdm start
rc-update add lightdm

# RDP & VPN
apk add freerdp
apk add openvpn

#Qt application packages
apk add networkmanager
apk add networkmanager-cli
apk add gnome-terminal
apk add gt5-qtbase-x11
lbu ci
#reboot
