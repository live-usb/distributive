#!/bin/ash

file=$(cat $1)

flag="client"
echo -n > /etc/openvpn/openvpn.conf

echo "$file" | while read line
do
if [[ "$line" == "<ca>" ]]; then
echo -n > /etc/openvpn/ca.crt
flag="ca"
fi

if [[ "$line" == "<cert>" ]]; then
echo -n > /etc/openvpn/cert.crt
flag="cert"
fi

if [[ "$line" == "<key>" ]]; then
echo -n > /etc/openvpn/ca.key
flag="key"
fi

if [[ "$line" == "<tls-auth>" ]]; then
echo -n > /etc/openvpn/ta.key
flag="tls"
fi

if [[ "$flag" == "client" ]]; then
echo "$line" >> /etc/openvpn/openvpn.conf
continue
fi

if [[ "$flag" == "ca" ]]; then
echo "$line" >> /etc/openvpn/ca.crt
continue
fi

if [[ "$flag" == "cert" ]]; then
echo "$line" >> /etc/openvpn/cert.crt
continue
fi

if [[ "$flag" == "key" ]]; then
echo "$line" >> /etc/openvpn/ca.key
continue
fi

if [[ "$flag" == "tls" ]]; then
echo "$line" >> /etc/openvpn/ta.key
continue
fi
done

