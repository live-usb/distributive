#!/bin/ash
. rdp.sh

if [[ "$VPN_USE" == "yes" ]]; then
	modprobe tun
	if [[ ! -f /etc/modules-load.d/tun.conf ]]; then
		echo "tun" >> /etc/modules-load.d/tun.conf
	fi
	
	./division.sh "$VPN_SETTINGS"
	rc-service openvpn start
	rc-update add openvpn	
fi
	
if [[ "$VPN_USE" == "no" ]]; then
	modprobe -r tun
	echo -n > /etc/modules-load.d/tun.conf
	rc-service openvpn stop
	rc-update del openvpn
fi
