#pragma once
#include <QFile>
#include <QDir>
#include <QString>
#include <QTextStream>
#include <QMessageBox>
#include <QSettings>
#include "mainwindow.h"
#include "ui_mainwindow.h"

QT_BEGIN_NAMESPACE namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE


class BaseConfigWorker
{
protected:
    MainWindow* mainWidget = nullptr;
    Ui::MainWindow* ui = nullptr;
    QString dirPath;
    QSettings* customConfig;
public:
    BaseConfigWorker(MainWindow* widget, QString dirPath):
        dirPath(dirPath),
        mainWidget(widget),
        ui(mainWidget->getUi())
    {
        this->customConfig = new QSettings(
                    QDir::homePath() + "/.config/distributive/remote-hosts.conf",
                    QSettings::IniFormat
        );
    }

    void write()
    {
        this->_createBaseDir();
        QString text = _prepareText();
        this->_writeConfig(text);
        this->_writeCustomConfig();
    }

protected:
    virtual QString _prepareText() = 0;
    virtual void _writeConfig(const QString& text) = 0;
    virtual void _writeCustomConfig() = 0;

    void _createBaseDir()
    {
        QDir dir(this->dirPath);

        if (!dir.exists())
            dir.mkpath(this->dirPath);

        return;
    }

    QMessageBox::StandardButton _showRewriteMessage(QString title, QString text)
    {
        return QMessageBox::question(
                    this->mainWidget,
                    title,
                    text,
                    QMessageBox::Yes | QMessageBox::No
                );
    }
};


class ConfigWorkerRDP : public BaseConfigWorker
{
public:
    ConfigWorkerRDP(MainWindow* mainWindow):
        BaseConfigWorker(
            mainWindow,
            QDir::homePath() + "/.config/distributive/rdp"
        )
    { }

private:
    void _writeConfig(const QString& text) override
    {
        QString fileName = ui->_edServerNameRDP->text() + ".rdp";
        QFile file(this->dirPath + "/" + fileName);

        if (file.exists()){
            auto reply = this->_showRewriteMessage(
                        "Warning",
                        "Файл с таким именем уже существует. Желаете перезаписать его?"
            );

            if (reply == QMessageBox::No)
                return;
        }

        if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream out(&file);
            out << text;
            file.close();
            QMessageBox::information(this->mainWidget, "Success","Конфигурация успешно записана");
        }
        else {
            QMessageBox::critical(this->mainWidget, "Error","Не удалось записать конфигурацию для подключения");
            return;
        }
    }

    QString _prepareText() override
    {
        QString config;
        if (!ui->_edUsernameRDP->text().isEmpty())
            config += QString("username:s:%1\n").arg(
                        ui->_edUsernameRDP->text());

        if (!ui->_edPasswordRDP->text().isEmpty())
            config += QString("password:s:%1\n").arg(
                        ui->_edPasswordRDP->text());

        config += ui->chboxUseDomainRDP->isChecked() ?
            QString("full address:s:%1:%2\n").arg(
                        ui->_edDomainRDP->text(), ui->_edPortRDP->text()) :
            QString("full address:s:%1:%2\n").arg(
                        ui->_edIPRDP->text(), ui->_edPortRDP->text());

        return config;
    }

    void _writeCustomConfig() override
    {
        customConfig->beginGroup(ui->_edServerNameRDP->text());
        customConfig->setValue("protocol", "RDP");
        customConfig->setValue("config", ui->_edServerNameRDP->text() + QString(".rdp"));
        if (ui->_btUseVPN->isChecked()) {
            customConfig->setValue("vpn_settings", ui->_edPathVPNConfig->text());
        }
        customConfig->endGroup();

        return;
    }
};


class ConfigWorkerSSH : public BaseConfigWorker
{
    QString* currentSection = nullptr;
    QStringList sections;
public:
    ConfigWorkerSSH(MainWindow* mainWindow):
        BaseConfigWorker(
            mainWindow,
            QDir::homePath() + "/.sssh"
        )
    { }

private:
    void _writeConfig(const QString& text) override
    {
        QFile file(this->dirPath + "/config");
        try {
            if (this->_checkServerExists(file)){
                auto reply = this->_showRewriteMessage(
                            "Warning",
                            "Сервер с таким именем уже существует. Перезаписать конфигурацию подключения?"
                        );

                if (reply == QMessageBox::No)
                    return;
            }
        }
        catch (const std::exception& e) {
            QMessageBox::critical(this->mainWidget, "Error", QString::fromStdString(e.what()));
            return;
        }

        if (this->currentSection)
            *this->currentSection = text;
        else
            sections.push_back(text);

        QString resultConfig = this->sections.join("");
        if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream out(&file);
            out << resultConfig;
            file.close();
            QMessageBox::information(this->mainWidget, "Success","Конфигурация успешно записана");
        }
        else {
            QMessageBox::critical(this->mainWidget, "Error","Не удалось записать конфигурацию для подключения");
            return;
        }

        return;
    }


    bool _checkServerExists(QFile& file)
    {
        if (!file.exists())
            return false;

        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream in(&file);
            QString contentConfig = in.readAll();
            file.close();

            this->sections = contentConfig.split("Host", Qt::SkipEmptyParts);

            for (QString& section : sections)
            {
                section = "Host" + section;
                auto searchHost = this->_getServerUrl();
                if (section.contains(searchHost)) {
                    this->currentSection = &section;
                }
            }

            if (this->currentSection)
                return true;

            return false;
        }

        throw std::runtime_error("Не удалось открыть файл конфигурации SSH");
    }

    QString _prepareText() override
    {
        QString config;
        config += _getServerUrl();

        config += QString(
                    "    User %1\n"
                    "    Port %2\n"
                    ).arg(ui->_edUsernameSSH->text(),ui->_edPortSSH->text());

        config += ui->cboxAuthorization->currentText() == "Password" ?
                    QString("    PasswordAuthentication yes"):
                    QString("    PasswordAuthentication no\n"
                            "    IdentityFile %1").arg(ui->_edPathRSAKey->text());

        return config + "\n\n";
    }

    QString _getServerUrl()
    {
        return ui->chboxUseDomainSSH->isChecked() ?
                    QString("Host %1\n").arg(ui->_edDomainSSH->text()) :
                    QString("Host %1\n").arg(ui->_edIPSSH->text());
    }

    void _writeCustomConfig() override
    {
        customConfig->beginGroup(ui->_edServerNameSSH->text());
        customConfig->setValue("protocol", "SSH");
        customConfig->setValue("url", this->_getServerUrl());
        if (ui->_btUseVPN->isChecked()) {
            customConfig->setValue("vpn_settings", ui->_edPathVPNConfig->text());
        }
        customConfig->endGroup();

        return;
    }
};
